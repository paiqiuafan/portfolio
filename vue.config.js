const path = require('path');
function resolve (dir) {
    return path.join(__dirname, dir)
}
process.env.VUE_APP_VERSION = require('./package.json').version;

module.exports = {
    chainWebpack: config => {
        config
        .plugin('html')
        .tap(args => {
            args[0].title= 'Wen Ching Yen'
            return args
        }),
        // 添加别名
        config.resolve.alias
            // .set('vue$', 'vue/dist/vue.esm.js')
            .set('@', resolve('src'))
            .set('@assets', resolve('src/assets'))
            .set('@V', resolve('src/views'))
            .set('@C', resolve('src/components'))
            .set('@U', resolve('src/utils'))
            .set('@M', resolve('src/mixin'))
            .set('@scss', resolve('src/assets/scss'))
            .set('@img', resolve('src/assets/images'))
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `
                    @import "@/assets/scss/main.scss";
                    @import "@/assets/scss/reset.scss";
                `
            }
        }
    }
};